FROM python:2.7
COPY server.py server.py
COPY config.json config.json
COPY TileStache/ /tilestache/
RUN pip install /tilestache
RUN pip install mapnik
EXPOSE 80/tcp
ENTRYPOINT [ "python", "server.py" ]