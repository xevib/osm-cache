import TileStache
import json
from werkzeug.serving import run_simple
import sys
import os

print("App id:{}".format(sys.argv[-1]))
os.environ['TILESTACHE_ID'] = sys.argv[-1]
tileserver = TileStache.WSGITileServer("./config.json", autoreload=False)
application = tileserver

run_simple('0.0.0.0', 80, application)